# git aliases

Some Git aliases for doing interesting things with your local repos and Bitbucket.

# Installation

## All of the aliases

    $ git clone https://bitbucket.org/tpettersen/git-aliases.git
    $ git config --global --add include.path "$(pwd)/git-aliases/.gitaliases"
   
## Some of the aliases

Copy and paste entiries from `.gitaliases` into your `~/.gitconfig`

# Aliases

## alias

`git alias <name> <command>`

A meta alias for creating a global aliases.

## ngrok

`git ngrok`

Serves your repository on the public internet over the `git://` protocol using 
[ngrok]. Requires that you have:

1. installed [jq]
1. installed [ngrok]

Note: the git:// protocol is read-only by default, but does not have any 
authentication built in, so anyone able to guess your ngrok TCP URL will be 
able to clone it. You could potentially use ngrok's [IP whitelisting] to 
mitigate this.

## serve

`git serve`

Used by `git ngrok`. This serves your repository from your local machine over 
the `git://` protocol. Adapted from [https://git.wiki.kernel.org/index.php/Aliases].

## pipeline

`git pipeline <pipeline-name> <commit-ish>...`

Triggers a custom [Bitbucket Pipeline] for the specified commit or commits. 
Requires that you have: 

1. installed [jq]
1. enabled Bitbucket Pipelines
1. defined at least one [custom pipeline]
1. locally set the `BBP_USER` environment variable to your Bitbucket username
1. locally set the `BBP_PASS` environment variable to a Bitbucket 
[app password] with the `Pipelines:Read` permission.

Note: a [commit-ish] is any Git object or ref that can be dereferenced to a 
commit, e.g. a tag, branch, commit, reflog shortname, ancestry reference, etc.

For example:

`git pipeline deploy master`

## pipelines

`git pipelines <pipeline-name> <revision range>`

Triggers a custom [Bitbucket Pipeline] for the specified range of commits. This
is particularly useful when attempting to hunt down a regression that was 
introduced within a range of commits.

For example, if you've just pushed some changes to `master`, then:

`git pipelines test origin/master{1}..origin/master{0}`

will run the `test` pipeline against every commit that you've just pushed.

## sta{0,3}sh

Stashes different types of changes in your worktree.

`git stsh` stash unstaged changes to tracked files

`git stash` ...plus unstaged changes to tracked files

`git staash` ...plus untracked files

`git staaash` ...plus ignored files

Note: `git staaash && git stash drop` is a great way to return your worktree to the 
default state.

## standup

`git standup`

`git lazy-standup`

`standup` displays a list of commit messages written by the current user since 
yesterday. `lazy-standup` pipes them to the `say` speech synthesis command. 

Requires that:

1. the $USER environment variable is defined
1. `say` is installed (stock on macOS)
1. you have committed some code in the last day

The latter is especially fun if you work remotely and can SSH into an on-site 
machine equipped with speakers (e.g. your team's wallboard).

## which

`git which <regex>`

Greps your local branches for one matching the specified regex. Used by 
`git lucky`.

## lucky

`git lucky <regex>`

Checks out the first branch matching the specified regex, 
*I'm feeling lucky* style.

[ngrok]: https://ngrok.com/
[jq]: https://stedolan.github.io/jq/
[IP whitelisting]: https://ngrok.com/docs#whitelist
[Bitbucket Pipeline]: https://bitbucket.org/product/features/pipelines
[custom pipeline]: https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html#Configurebitbucket-pipelines.yml-ci_customcustom(optional)
[app password]: https://bitbucket.org/account/admin/app-passwords
[commit-ish]: https://www.kernel.org/pub/software/scm/git/docs/gitglossary.html#def_commit-ish